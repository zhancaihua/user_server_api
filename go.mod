module gitee.com/zhancaihua/user_server_api

go 1.20

//protoc 22.2
//google.golang.org/protobuf v1.28.1 protoc-gen-go[google.golang.org/protobuf v1.28.1 内部的main package] 1.28.1
//google.golang.org/grpc v1.53.0 protoc-gen-go-grpc 1.3.0[innner module]
require (
	google.golang.org/grpc v1.53.0
	google.golang.org/protobuf v1.28.1
)

require github.com/envoyproxy/protoc-gen-validate v0.9.1

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20230110181048-76db0878b65f // indirect
)

retract v1.0.0
