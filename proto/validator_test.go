package proto

import (
	"testing"
)

func TestMobileRequestValidator(t *testing.T) {
	mr := &MobileRequest{
		Mobile: "13556789807",
	}
	if err := mr.Validate(); nil != err {
		t.Fatal(err)
	}
}
